const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const http = require('http');

mongoose.set('useCreateIndex', true);

const routes = require('./routes');
const { setupWebsocket } = require('./websocket')

const app = express();
const server = http.Server(app);



app.get('/', (req, res) =>{
    return res.send('esta funcionando');
});

setupWebsocket(server);

mongoose.connect('mongodb+srv://bussi:ss1029384756@forestudy-8rhl2.mongodb.net/week10?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

app.use(cors());
app.use(express.json());
app.use(routes);

server.listen(3333);
